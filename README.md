cryxor2014
==========

The 3rd version of Cryxor, by Arthur POULET. This application encrypt your datas with the one-time pad algorithm. 

Before to use
------

``
  sudo ln -s libcryxor.so /usr/bin/libcryxor.so.1.0
  make
  ./cryxor2014
``
